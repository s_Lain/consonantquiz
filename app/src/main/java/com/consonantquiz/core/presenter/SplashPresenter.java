package com.consonantquiz.core.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.consonantquiz.common.backend.firebase.GoogleAuthenticationManager;
import com.consonantquiz.common.base.BasePresenter;
import com.consonantquiz.common.events.AuthStateEvent;
import com.consonantquiz.common.events.BusProvider;
import com.consonantquiz.common.exception.IllegalActivityConnectedException;
import com.consonantquiz.core.view.SplashActivity;
import com.squareup.otto.Subscribe;

/**
 * Created by admin on 2017-03-24.
 */

public class SplashPresenter implements BasePresenter {
    final int SIGN_IN = 9001; // for GoogleSignIn Intent
    SplashActivity activity;
    GoogleAuthenticationManager authManager;

    @Override
    public void setView(AppCompatActivity activity) {
        if(activity instanceof SplashActivity){
            this.activity = (SplashActivity) activity;
            authManager = GoogleAuthenticationManager.getInstance();
            authManager.init(activity);
            BusProvider.getInstance().register(this);
        } else throw new IllegalActivityConnectedException("SplashPresenter");
    }

    @Override
    public void unsetView() {

    }

    public Intent getSignInIntent() {
        return authManager.getSignInIntent();
    }

    public boolean checkLoginResultFromIntent(Intent data){
        return authManager.checkLoginResultFromIntent(data, activity);
    }

    @Subscribe
    public void loginSubscriber(AuthStateEvent e){
        if(!e.isLogin) activity.showSignInButton();
        else{
            activity.startMainActivity();
        }
    }
}
