package com.consonantquiz.core.presenter;

import android.support.v7.app.AppCompatActivity;

import com.consonantquiz.common.base.BasePresenter;
import com.consonantquiz.common.exception.IllegalActivityConnectedException;
import com.consonantquiz.core.view.RoomActivity;

/**
 * Created by admin on 2017-03-25.
 */

public class RoomPresenter implements BasePresenter {
    RoomActivity activity;

    @Override
    public void setView(AppCompatActivity activity) {
        if (activity instanceof RoomActivity) {
            this.activity = (RoomActivity) activity;
            //BusProvider.getInstance().register(this);
        } else throw new IllegalActivityConnectedException("Illegal View-Presenter Connection!");
    }

    @Override
    public void unsetView() {
    }

    public void sendMessage(String message) {

    }

    public void connection(final String roomID) {

    }

    private void enterChannel(final String roomID) {

    }
}
