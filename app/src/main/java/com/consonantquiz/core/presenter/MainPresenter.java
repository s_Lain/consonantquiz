package com.consonantquiz.core.presenter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.consonantquiz.common.backend.firebase.DatabaseManager;
import com.consonantquiz.common.backend.firebase.GoogleAuthenticationManager;
import com.consonantquiz.common.base.BasePresenter;
import com.consonantquiz.common.base.adapter.RoomListAdapter;
import com.consonantquiz.common.events.BusProvider;
import com.consonantquiz.common.events.RoomListChangedEvent;
import com.consonantquiz.common.exception.IllegalActivityConnectedException;
import com.consonantquiz.core.view.MainActivity;
import com.squareup.otto.Subscribe;

/**
 * Created by admin on 2017-03-24.
 */

public class MainPresenter implements BasePresenter {
    MainActivity activity;
    DatabaseManager dbManager;

    RoomListAdapter roomListAdapter;
    LinearLayoutManager roomListLayoutManager;

    @Override
    public void setView(AppCompatActivity activity) {
        if(activity instanceof MainActivity){
            this.activity = (MainActivity) activity;
            dbManager = DatabaseManager.getInstance();
            BusProvider.getInstance().register(this);
        } else throw new IllegalActivityConnectedException("Illegal View-Presenter Connection!");
    }

    @Override
    public void unsetView() {
    }


    public void setRecyclerView(RecyclerView view){
        roomListAdapter = new RoomListAdapter();
        roomListLayoutManager = new LinearLayoutManager(activity);
        view.setAdapter(roomListAdapter);
        view.setLayoutManager(roomListLayoutManager);
    }

    public void logout(){
        GoogleAuthenticationManager.getInstance().logout();
    }

    public void createRoom(){
    }

    public void createRoom(String initialQuestion, String initialGenre){
        dbManager.createRoom(initialQuestion, initialGenre);
    }

    //TODO move to MainActivity (not presenter = input onstart and onpause) <- for activity's life cycler
    @Subscribe
    public void roomListChangedEvent(RoomListChangedEvent e){
        roomListAdapter.setRoomList(e.roomList);
    }
}
