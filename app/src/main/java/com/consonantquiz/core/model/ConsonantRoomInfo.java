package com.consonantquiz.core.model;

/**
 * Created by admin on 2017-03-25.
 */

//RoomID는 Key로서 사용된다.
public class ConsonantRoomInfo {
    public long roomID;

    public String currentQuestion;
    public String currentGenre;
    public String examinerID;

    public ConsonantRoomInfo(){

    }

    public ConsonantRoomInfo(String question, String genre, long roomID, String examinerID){
        currentGenre = genre;
        currentQuestion = question;
        this.roomID = roomID;
        this.examinerID = examinerID;
    }
}
