package com.consonantquiz.core.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.consonantquiz.R;
import com.consonantquiz.core.presenter.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.main_rooms_recyclerview)
    RecyclerView roomRecyclerView;

    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter();
        presenter.setView(this);
        initView();
    }

    @Override
    protected void onDestroy() {
        presenter.unsetView();
        super.onDestroy();
    }

    private void initView(){
        presenter.setRecyclerView(roomRecyclerView);
    }

    @OnClick({R.id.main_create_button, R.id.main_entrance_button, R.id.main_quick_button})
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.main_create_button:
                presenter.createRoom("abc","cde");
                //Intent toRoomActivity = new Intent(this, RoomActivity.class);

                break;
            case R.id.main_entrance_button:
                startActivity(new Intent(this, RoomActivity.class));
                break;
            case R.id.main_quick_button:
                presenter.logout();
                Toast.makeText(this, "logout", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
