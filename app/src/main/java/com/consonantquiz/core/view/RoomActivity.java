package com.consonantquiz.core.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.consonantquiz.R;
import com.consonantquiz.core.presenter.RoomPresenter;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoomActivity extends AppCompatActivity implements View.OnClickListener{
    RoomPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        ButterKnife.bind(this);

        presenter = new RoomPresenter();
        presenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @OnClick({R.id.room_chat_send_button})
    @Override
    public void onClick(View view) {

    }
}
