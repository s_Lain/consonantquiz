package com.consonantquiz.core.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.consonantquiz.R;
import com.consonantquiz.core.presenter.SplashPresenter;
import com.google.android.gms.common.SignInButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {
    final int SIGN_IN = 9001; // for GoogleSignIn Intent
    @BindView(R.id.splash_google_login_button)
    SignInButton signInButton;

    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        presenter = new SplashPresenter();
        presenter.setView(this);
    }

    public void showSignInButton(){
        signInButton.setVisibility(View.VISIBLE);
    }

    public void startMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.unsetView();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN) {
            presenter.checkLoginResultFromIntent(data);
        }
    }

    @OnClick({R.id.splash_google_login_button})
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.splash_google_login_button:
                startActivityForResult(presenter.getSignInIntent(), SIGN_IN);
                break;
        }
    }
}
