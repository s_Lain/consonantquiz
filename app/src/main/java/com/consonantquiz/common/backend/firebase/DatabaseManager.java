package com.consonantquiz.common.backend.firebase;

import android.util.Log;

import com.consonantquiz.common.events.BusProvider;
import com.consonantquiz.common.events.RoomListChangedEvent;
import com.consonantquiz.core.model.ConsonantRoomInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017-03-25.
 * 파이어베이스 데이터베이스 관리자.
 */

public class DatabaseManager {
    private static volatile DatabaseManager instance;

    public static DatabaseManager getInstance() {
        if (instance == null) {
            synchronized (DatabaseManager.class) {
                if (instance == null) {
                    instance = new DatabaseManager();
                }
            }
        }
        return instance;
    }

    private GoogleAuthenticationManager authManager;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference roomsReference;
    private DatabaseReference usersInRoomReference; // 방에 접속하였을 경우
    private List<ConsonantRoomInfo> roomList;

    //싱글톤이므로 한번만 실행된다다
    private DatabaseManager() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        roomsReference = firebaseDatabase.getReference("rooms"); // 방목록은 언제나 필요하다.
        initDatabaseManager();
        roomList = null;
        authManager = GoogleAuthenticationManager.getInstance();
    }

    public void createRoom(String initialQuestion, String initialGenre) {
        long roomID = System.currentTimeMillis(); // 사실 없어도 된다.
        ConsonantRoomInfo roomInfo = new ConsonantRoomInfo(initialQuestion, initialGenre, roomID, authManager.getUserUid());
        roomsReference.child(String.valueOf(roomID)).setValue(roomInfo);
    }

    //Application Level에서 실행되며, 다양한 실시간반응을 등록한다.
    //반응은 이벤트버스에의해 발행된다.
    public void initDatabaseManager() {
        registCategoryListener();
    }

    private void registCategoryListener() {
        /**
         * 방목록을 언제나 갱신한다. 갱신한 방목록은 구독위치에 뿌려진다.
         * 구독을 원하지 않는 경우에도 갱신하기 때문에, 이부분에서 remove가 일어나지 않고,
         * Activity Life Cycle에 따라 액티비티가 onStart 한 경우 register, onPause(스택뒤로밀림)의 경우 unregister 한다.
         */
        roomsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                roomList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ConsonantRoomInfo roomInfo = snapshot.getValue(ConsonantRoomInfo.class);
                    //long roomID = Long.valueOf(snapshot.getKey());

                    roomList.add(roomInfo);
                }

                BusProvider.getInstance().post(new RoomListChangedEvent(roomList));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                roomList = null;
                Log.d("Database", databaseError.getMessage());
                // onCancelled
            }
        });
    }
}
