package com.consonantquiz.common.backend.firebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.consonantquiz.R;
import com.consonantquiz.common.events.AuthStateEvent;
import com.consonantquiz.common.events.BusProvider;
import com.consonantquiz.core.view.SplashActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

/**
 * Created by admin on 2017-03-24.
 */

public class GoogleAuthenticationManager {
    private static volatile GoogleAuthenticationManager instance;
    public static GoogleAuthenticationManager getInstance(){
        if(instance == null){
            synchronized (GoogleAuthenticationManager.class){
                if(instance == null){
                    instance = new GoogleAuthenticationManager();
                }
            }
        }
        return instance;
    }
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;

    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;

    private GoogleSignInAccount account;

    //Splash Activity
    public void init(AppCompatActivity mActivity){
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mActivity.getString(R.string.GOOGLE_AUTH_WEB_CLIENT_ID))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .enableAutoManage(mActivity, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser = null;

        //FirebaseAuth는 현재상태의 변경이 있을때 바로바로 적용된다.
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mFirebaseUser = firebaseAuth.getCurrentUser();
                if (mFirebaseUser != null) {
                    BusProvider.getInstance().post(new AuthStateEvent(true, mFirebaseUser));
                    // User is signed in
                } else {
                    BusProvider.getInstance().post(new AuthStateEvent(false, null));
                    // User is signed out
                }
            }
        });
    }
    /**
     * 해당 결과를 가지고 startActivityForResult를 보낸다.
     * @return 로그인화면 Intent 를 반환한다.
     */
    public Intent getSignInIntent(){
        return Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
    }

    public boolean checkLoginResultFromIntent(Intent data, SplashActivity activity){
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            // Google Sign In was successful, authenticate with Firebase
            account = result.getSignInAccount();

            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {

                            //Firebase, Google Login Integration Failed
                        }
                        mFirebaseUser = task.getResult().getUser();

                        // ...
                    }
                });

        } else {
            // Google Sign In failed, update UI appropriately
            // ...
        }
        return true;
    }

    private GoogleAuthenticationManager(){}

    public String getUserUid(){
        if(mFirebaseUser == null) return null;
        else return mFirebaseUser.getUid();
    }

    public void logout() {
        mAuth.signOut();
    }
}
