package com.consonantquiz.common.events;


import com.squareup.otto.Bus;

/**
 * Created by admin on 2017-03-23.
 * 이벤트를 제어하는 클래스. 특별히 하는 일은 없다.
 */

public class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}
