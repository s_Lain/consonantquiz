package com.consonantquiz.common.events;

import com.consonantquiz.core.model.ConsonantRoomInfo;

import java.util.List;

/**
 * Created by admin on 2017-03-25.
 */

public class RoomListChangedEvent {
    public List<ConsonantRoomInfo> roomList;
    public boolean isAdded;


    public RoomListChangedEvent(List<ConsonantRoomInfo> roomList){
        this.roomList = roomList;
    }

    public void setAdded(boolean isAdded){
        this.isAdded = isAdded;
    }
}
