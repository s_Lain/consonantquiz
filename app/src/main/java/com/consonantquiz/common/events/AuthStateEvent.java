package com.consonantquiz.common.events;

import com.google.firebase.auth.FirebaseUser;

/**
 * Created by admin on 2017-03-24.
 * 로그인 결과의 변경점이 있을때 발행된다.
 * - 발행처 : GoogleAuthenticationManager.class
 * - 발행시 : 로그인정보 확인, 로그인정보 변경
 *
 * - 구독처 : SplashPresenter(스플래시 이미지로 대기중, 로그인정보 없을시 로그인버튼 활성, 있을시 메인으로 이동)
 */

public class AuthStateEvent {
    public boolean isLogin;
    FirebaseUser userInformation; // if isLogin == false then null

    public AuthStateEvent(boolean isLogin, FirebaseUser user){
        this.isLogin = isLogin;
        this.userInformation = user;
    }
}
