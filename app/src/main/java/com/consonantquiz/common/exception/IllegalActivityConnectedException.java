package com.consonantquiz.common.exception;

import android.util.Log;

/**
 * Created by admin on 2017-03-24.
 */

public class IllegalActivityConnectedException extends RuntimeException {
    public IllegalActivityConnectedException(String msg){
        Log.e("CUSTOM_EXCEPTION", msg);
    }
}
