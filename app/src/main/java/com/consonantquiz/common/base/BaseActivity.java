package com.consonantquiz.common.base;

/**
 * Created by admin on 2017-03-24.
 */

public interface BaseActivity {
    void setPresenter(BasePresenter presenter);

}
