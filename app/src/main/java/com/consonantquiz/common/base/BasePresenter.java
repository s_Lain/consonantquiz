package com.consonantquiz.common.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by admin on 2017-03-24.
 */

public interface BasePresenter {
    void setView(AppCompatActivity activity);
    void unsetView();
}
