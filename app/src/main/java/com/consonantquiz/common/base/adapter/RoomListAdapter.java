package com.consonantquiz.common.base.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.consonantquiz.R;
import com.consonantquiz.core.model.ConsonantRoomInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 2017-03-25.
 */

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder> {
    ArrayList<ConsonantRoomInfo> roomList;

    public RoomListAdapter(){
        roomList = new ArrayList<>();
    }

    public RoomListAdapter(List<ConsonantRoomInfo> roomList){
        setRoomList(roomList);
    }

    public void setRoomList(List<ConsonantRoomInfo> roomList){
        if(roomList instanceof ArrayList)
            this.roomList = (ArrayList<ConsonantRoomInfo>) roomList;
        else this.roomList = new ArrayList<>(roomList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_main_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.question.setText(roomList.get(position).currentQuestion);
        holder.genre.setText(roomList.get(position).currentGenre);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long dummy = roomList.get(position).roomID;
            }
        });
    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_main_room_current_question_textview)
        TextView question;
        @BindView(R.id.item_main_room_current_genre_textview)
        TextView genre;
        @BindView(R.id.item_main_room_container)
        CardView itemLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
