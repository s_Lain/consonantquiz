package com.consonantquiz.common.base;

import android.app.Application;
import android.content.Intent;

import com.consonantquiz.common.events.AuthStateEvent;
import com.consonantquiz.common.events.BusProvider;
import com.consonantquiz.core.view.SplashActivity;
import com.squareup.otto.Subscribe;

/**
 * Created by admin on 2017-03-24.
 */

public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onLogoutWhilePlaying(AuthStateEvent e){
        if(!e.isLogin){
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
